import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class test2 {

    private static final Logger logger = LogManager.getLogger(HelloWorld.class);
    @Test
    public  void test2() {
        BasicConfigurator.configure();
        logger.info("Hello from profile 2");
    }
}
