import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class HelloWorld {

    private static final Logger logger = LogManager.getLogger(HelloWorld.class);
    public static void main(String[] args) {
        // basic log4j configurator
        BasicConfigurator.configure();
        String prop = System.getProperty("hello.world");
        logger.info(prop);


    }

}  